from ..kernel import Monster, Attribute

class BlueEyesWhiteDragon(Monster):
    "Blue-Eyes White Dragon"
    def __init__(self):
        ""
        super().__init__(
            name = 'Blue-Eyes White Dragon',
            description = (
                'This legendary dragon is a powerful engine of destruction.'
                'Virtually invincible, very few have faced this awesome'
                'creature and lived to tell the tale.'
            ),
            types = frozenset(['dragon']),
            ATK = 3000,
            DEF = 2500,
            level = 8,
            attribute = Attribute.Light
        )

export = BlueEyesWhiteDragon
