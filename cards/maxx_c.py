"""
Maxx_"C"
ref: http://yugioh.wikia.com/wiki/Maxx_%22C%22
"""

from ..kernel import Monster, Attribute, Effect

def passs():
    return True

class Activate(Effect):
    'Activate MaxxC Effect'
    def __init__(self, main: Main):

        def negate_main():
            main.negated = True

        def resolve():
            main.negated = False
            self.card.side.board.listen('end-phase', negate_main)

        super().__init__(
            name = 'Maxx "C"',
            events = {'chain'},
            check = passs,
            resolve = resolve
        )

class Main(Effect):
    'Main MaxxC Effect'
    def __init__(self):

        def resolve():
            self.card.side.draw(1)

        super().__init__(
            name = 'Maxx "C"',
            events = {'summon:special'},
            check = passs,
            resolve = resolve
        )

class MaxxC(Monster):
    'Maxx "C"'
    def __init__(self):

        effect__main = Main()
        effect__activate = Activate(effect_main)

        super().__init__(
            name = 'Maxx "C"'
            description = ("During either player's turn: You can send this card"
                           " from your hand to the Graveyard; this turn, each"
                           " time your opponent Special Summons a monster(s), " 
                           "immediately draw 1 card. You can only use 1" 
                           '"Maxx "C"" per turn.'),
            types = frozenset(['insect']),
            ATK = 500,
            DEF = 200,
            level = 2,
            attribute = Attribute.Earth,
            effects = Set([effect__activate, effect__main])
        )
