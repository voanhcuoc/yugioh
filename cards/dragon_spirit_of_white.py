"""
Dragon Spirit of White
ref: http://yugioh.wikia.com/wiki/Dragon_Spirit_of_White
"""

from ..kernel import Monster, Attribute, Effect
from .blue_eyes_white_dragon import BlueEyesWhiteDragon

def passs():
    return True

class Normal(Effect):
    def __init__(self):

        def check():
            return ((self.card in self.card.side.hand) or 
                    (self.card in self.card.side.graveyard))

        super().__init__(
            name = 'Dragon Spirit of White',
            events = {},
            check = check,
            resolve = passs
        )

    def pipe__types(self, types):
        return types.difference(['effect']).union(['normal'])

class Banish(Effect):
    def __init__(self):

        def this(card, *args):
            return card is self.card

        def resolve():
            target = self.targets[0]
            self.card.side.opponent.banish(target)

        super().__init__(
            name = 'Dragon Spirit of White',
            events = {'summon:normal', 'summon:special'},
            check = this,
            resolve = resolve
        )

class Summon(Effect):
    def __init__(self):

        def check():
            return (bool(self.card.side.opponent.monster_zone) and
                    BlueEyesWhiteDragon in map(type, self.card.side.hand))

        def resolve(bewg):
            self.card.side.special_summon(bewg, [self.card])

        super().__init__(
            name = 'Dragon Spirit of White',
            events = {'chain'},
            check = check,
            resolve = resolve
        )

class DragonSpiritOfWhite(Monster):
    def __init__(self):
        super().__init__(
            name = 'Dragon Spirit of White',
            description = ('(This card is always treated as a "Blue-Eyes" card'
                           '.)\nThis card is treated as a Normal Monster while'
                           'in the hand or Graveyard. When this card is Normal'
                           'or Special Summoned: You can target 1 Spell/Trap'
                           'Card your opponent controls; banish it. During'
                           'either player\'s turn, if your opponent controls a'
                           'monster: You can Tribute this card; Special Summon'
                           '1 "Blue-Eyes White Dragon" from your hand.'),
            types = frozenset(['dragon']),
            ATK = 2500,
            DEF = 2000,
            level = 8,
            attribute = Attribute.Light,
            effect = [Normal(), Banish(), Summon()]
        )
