from abc import ABC, abstractmethod
from typing import Set, Callable, List, Iterator, FrozenSet
from enum import Enum, auto

class Attribute(Enum):
    Dark = auto()
    Light = auto()
    Fire = auto()
    Water = auto()
    Earth = auto()
    Wind = auto()

class Effect(ABC):
    """
    Generic Effect.
    Implement this to define an effect.
    """
    @abstractmethod
    def __init__(self,
                 name: str,
                 events: Set[str],
                 check: Callable[[], bool],
                 resolve: Callable) -> None:
        """
        :param name:    name of this effect that will be refered by other
                        effects
        :param events:  the effect is hooked to these events, ``check`` will be
                        called if event name found here
        :param check:   function performs condition checking
        :param resolve: function actually resolves this effect
        """
        self.name: str
        self.events: Set[str]
        self.check: Callable[[], bool]
        self.resolve: Callable
        self.card: 'Card'
        self.negated: bool
        self.targets: List['Card']

    @abstractmethod
    def pipe__ATK(self, ATK: int) -> int:
        """
        Monsters targeted by effects have their ATK pipe through this method
        of these effects, one after the other

        :param ATK: ATK before effect
        :return:    ATK after effect
        """

    @abstractmethod
    def pipe__DEF(self, DEF: int) -> int:
        """
        Monsters targeted by effects have their DEF pipe through this method
        of these effects, one after the other

        :param DEF: DEF before effect
        :return:    DEF after effect
        """

    @abstractmethod
    def pipe__types(self, types: FrozenSet[str]) -> FrozenSet[str]:
        """
        Monsters targeted by effects have their types pipe through this method
        of these effects, one after the other

        :param types: types before effect
        :return:      types after effect
        """

    @abstractmethod
    def pipe__level(self, level: int) -> int:
        """
        Monsters targeted by effects have their level pipe through this method
        of these effects, one after the other

        :param level: level before effect
        :return:      level after effect
        """

    @abstractmethod
    def pipe__attribute(self, attribute: Attribute) -> Attribute:
        """
        Monsters targeted by effects have their attribute pipe through this
        method of these effects, one after the other

        :param attribute: attribute before effect
        :return:          attribute after effect
        """

    @abstractmethod
    def target(self, targets: 'List[Card]'):
        """
        Mark a bunch of :class:`Card` as targets of this :class:`Effect`

        :param targets: targets to add
        """

    @abstractmethod
    def re_target(self, new: 'Card', old: 'Card' = None):
        """
        Switch effect to a new target

        :param new: new target
        :param old: old target that is no longer a target
        """

    @abstractmethod
    def remove_target(self, target: 'Card' = None):
        """
        Remove a specific card from being a target of this effect

        :param target: card that is currently a target
        """

# TODO class Condition: http://yugioh.wikia.com/wiki/Condition

class Card(ABC):
    """
    Generic Card.
    To define a Card, please implement :class:`Monster`, :class:`Spell` or
    :class:`Trap` instead.
    """
    @abstractmethod
    def __init__(self,
                 name: str,
                 description: str,
                 types: FrozenSet[str],
                 effects: List[Effect]) -> None:
        """
        :param name:        Card Name
        :param description: Card Description
        :param types:       monster types or spell or trap
        :param effects:     Card :class:`Effect`
        """
        self.name: str
        self.description: str
        self.types: FrozenSet[str]
        self.effects: List[Effect]
        self.face: str
        self.alternative_names: List[str]
        self.targeters: List[Effect]

    @property
    @abstractmethod
    def current__types(self):
        """
        Get current types of this card, that affected by the effects
        targeting it.
        """

class Monster(Card):
    """
    Generic Monster Card.
    Implement this to define a Monster Card.
    """
    @abstractmethod
    def __init__(self,
                 name: str,
                 description: str,
                 types: FrozenSet[str],
                 ATK: int,
                 DEF: int,
                 level: int,
                 attribute: Attribute,
                 effects: List[Effect] = []) -> None:
        """
        :param name:        Card Name
        :param description: Card Description
        :param types:       Monster Types
        :param effects:     Card :class:`Effect`
        :param ATK:         original ATK (constant)
        :param DEF:         original DEF (constant)
        :param level:       Monster Level (number of stars on the card)
        :param attribute:   Monster Attribute
        """
        self.ATK: int
        self.DEF: int
        self.level: int
        self.attribute: Attribute
        self.targeter: List[Effect]
        self.face: str     # 'up'     or 'down'
        self.position: str # 'attack' or 'defense'

    @property
    @abstractmethod
    def current__ATK(self):
        """
        Get current ATK of this monster, that affected by the effects
        targeting it.
        """

    @property
    @abstractmethod
    def current__DEF(self):
        """
        Get current DEF of this monster, that affected by the effects
        targeting it.
        """

    @property
    @abstractmethod
    def current__types(self):
        """
        Get current types of this monster, that affected by the effects
        targeting it.
        """

    @property
    @abstractmethod
    def current__level(self):
        """
        Get current level of this monster, that affected by the effects
        targeting it.
        """

    @property
    @abstractmethod
    def current__attribute(self):
        """
        Get current attribute of this monster, that affected by the effects
        targeting it.
        """

class Spell(Card):
    """
    Generic Spell Card.
    Implement this to define a Spell Card.
    """
    @abstractmethod
    def __init__(self,
                 name: str,
                 description: str,
                 icon: str,
                 effects: List[Effect]) -> None:
        """
        :param name:        Card Name
        :param description: Card Description
        :param icons:       'equip' or 'field' or 'quick' or
                            'ritual' or 'continuous' or 'counter'
        :param effects:     Card :class:`Effect`
        """

class Trap(Card):
    """
    Generic Trap Card.
    Implement this to define a Trap Card.
    """
    @abstractmethod
    def __init__(self,
                 name: str,
                 description: str,
                 icon: str,
                 effects: List[Effect]) -> None:
        """
        :param name:        Card Name
        :param description: Card Description
        :param icons:       'equip' or 'field' or 'quick' or
                            'ritual' or 'continuous' or 'counter'
        :param effects:     Card :class:`Effect`
        """

class Side(ABC):
    """
    Side of the Board. Represent a player with his own state and behavior.
    
    Methods which simulating player behavior here won't perform a check.
    :class:`Condition` for each method to happen are assumed to be met.
    """
    @abstractmethod
    def __init__(self):
        self.board: 'Board'
        self.opponent: Side

    @abstractmethod
    def side_of_the_field(self) -> Iterator[Card]:
        """
        Iterator that yield all cards on this
        *side of the field* i.e. cards in Monster Zone,
        Spell Trap Zone, Field Card Zone.

        :yield: :class:`Card`
        """

    @abstractmethod
    def effectual_cards(self) -> Iterator[Card]:
        """
        Iterator that yield all card on this side of the board
        that can activate or being targeted by an effect i.e.
        all cards on side except those in the deck.

        :yield: :class:`Card`
        """

    @abstractmethod
    def normal_summon(self,
                      summoned_monster: Monster,
                      tributed_monsters: List[Monster] = []):
        """
        Normal summon a :class:`Monster` in face-up attack position
        (optionally by tributed).

        :param summoned_monster:  Monster to be summoned
        :param tributed_monsters: Monster to be tributed
        """

    @abstractmethod
    def set(self,
            set_monster: Monster,
            tributed_monsters: List[Monster] = []):
        """
        Set a :class:`Monster` in face-down defense position
        (optionally by tributed).

        :param summoned_monster:  Monster to be summoned
        :param tributed_monsters: Monster to be tributed
        """

    @abstractmethod
    def remove_from_field(self, card: Card):
        """
        Remove a :class:`Card` from the field
        (resulted in being destroyed, etc.).

        :param card: Card to be removed
        """

    @abstractmethod
    def destroy(self, card: Card):
        """
        Destroy a :class:`Card`, in battle or by card effect.

        :param card: Card to be destroyed
        """

    @abstractmethod
    def inflict_damage(self, inflicted_life_point: int):
        """
        Inflict damage to ``self``.

        :param inflicted_life_point: Inflicted LP
        """

    @abstractmethod
    def activate(self,
                 effect: Effect):
        """
        Activate a card effect, append it to resolution stack.

        :param effect: Effect to be activated
        """
        # FIXME In case of actually perform the activation,
        #       should the chain already be specified ?

    @abstractmethod
    def resolve(self):
        """
        Resolve all effects that have been activated (i.e. effects in
        resolution stack), then clear resolution stack.
        """

    @abstractmethod
    def flip(self, card: Card):
        """
        Flip a card on field e.g. by flip summon or being attacked
        or by card effect.

        :param card: Card to be flipped
        """

    @abstractmethod
    def attack(self,
               attacker: Monster,
               defender: Monster):
        """
        Perform a battle step.

        :param attacker: the Monster declare battle
        :param defender: the Monster be attacked
        """
        # FIXME Should we name this method 'battle' ?

    @abstractmethod
    def send_to_graveyard(self, card: Card):
        """
        Send a Card to graveyard e.g. after destroyed by battle or by discard.

        :param card: Card to be sent
        """

    @abstractmethod
    def banish(self, card: Card):
        """
        Banish a Card from play.

        :param card: Card to be banished
        """

class Board(ABC):
    """
    The Board. Represent state of the match.

    Cards take effect at this scope.
    """
    @abstractmethod
    def __init__(self,
                 side_top: Side,
                 side_bot: Side) -> None:
        """
        Specify both sides (both players) of this board.

        :param side_top: first Side
        :param side_bot: second Side
        """
        self.side_top: Side
        self.side_bot: Side

    @abstractmethod
    def field(self) -> Iterator[Card]:
        """
        Yield all cards on the field i.e. both sides of the field.

        :yield: :class:`Card`
        """

    @abstractmethod
    def effectual_cards(self) -> Iterator[Card]:
        """
        Yield all effectual cards. See :method:`Side.effectual_cards`.

        :yield: :class:`Card`
        """

    @abstractmethod
    def listen(self, event_name: str, callback: Callable):
        """
        Attach callback as a listener to the event specified.

        :param event_name: Name of the event
        :param callback:   Callback function
        """

    @abstractmethod
    def trigger(self, event_name: str, *args):
        """
        Trigger an event e.g. gaming phase, step or card effect.
        Invoke all callbacks listen to the event, in appending order.

        :param event_name: Name of the event
        :param *args:      Params that are passed to callbacks
        """
