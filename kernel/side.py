from typing import List

from . import abstract

class Side(abstract.Side):
    def __init__(self):
        self.monster_zone: List[abstract.Card] = []
        self.spell_trap_zone: List[abstract.Card] = []
        self.field_card: Spell = None
        self.graveyard: List[abstract.Card] = []
        self.deck: List[abstract.Card] = []
        self.extra_deck: List[abstract.Card] = []
        self.banished: List[abstract.Card] = []
        self.hand: List[abstract.Card] = []
        self.life_point: int = 8000
        self.board: Board
        self.opponent: Side
        self._resolution_stack: List[abstract.Effect] = []

    def side_of_the_field(self):
        # `return self.monster_zone + self.spell_trap_zone` will mislead
        # caller to .remove(card) on it, which will not remove that card
        # from the respective zone
        for card in self.monster_zone:
            yield card

        for card in self.spell_trap_zone:
            yield card

        yield self.field_card

    def effectual_cards(self):
        # Cards on the field, graveyard, hand, which its effect can be triggered
        for card in self.side_of_the_field():
            yield card

        for card in self.graveyard:
            yield card

        for card in self.banished:
            yield card

    def normal_summon(self, summoned_monster: abstract.Monster,
                      tributed_monsters: List[abstract.Monster] = []):
        # Move tributed monsters from the field to graveyard
        map(self.monster_zone.remove, tributed_monsters)
        self.graveyard.extend(tributed_monsters)

        # Move summoned monsters from hand to field
        self.hand.remove(summoned_monster)
        self.monster_zone.append(summoned_monster)
        summoned_monster.face = 'up'
        summoned_monster.position = 'attack'

        # Trigger events
        self.board.trigger('summon:normal', summoned_monster)
        if tributed_monsters:
            self.board.trigger('summon:normal:tribute',
                               summoned_monster, tributed_monsters)

    def set(self, set_monster: abstract.Monster,
            tributed_monsters: List[abstract.Monster] = []):
        # Move tributed monsters from the field to graveyard
        map(self.monster_zone.remove, tributed_monsters)
        self.graveyard.extend(tributed_monsters)

        # Move summoned monsters from hand to field
        self.hand.remove(set_monster)
        self.monster_zone.append(set_monster)
        set_monster.face = 'down'
        set_monster.position = 'defense'

        # Trigger events
        self.board.trigger('set', set_monster)
        if tributed_monsters:
            self.board.trigger('set:tribute', set_monster, tributed_monsters)

    def remove_from_field(self, card: abstract.Card):
        self.board.trigger('remove-from-field', card)
        self.monster_zone.remove(card)
        self.spell_trap_zone.remove(card)
        if card == self.field_card:
            self.field_card = None

    def destroy(self, card: abstract.Card):
        # Move destroyed card to graveyard
        self.remove_from_field(card)
        self.graveyard.append(card)

        # Trigger events
        self.board.trigger('destroy', card)

    def inflict_damage(self, inflicted_life_point: int):
        self.life_point -= inflicted_life_point
        self.board.trigger('damage', inflicted_life_point)

    def activate(self, effect: abstract.Effect):
        card = effect.card
        self._resolution_stack.append(effect)
        self.board.trigger('activate', card, effect)

    def resolve(self):
        for effect in reverse(self._resolution_stack):
            if not effect.negated and effect.check():
                effect.resolve()
        self._resolution_stack = []

    def flip(self, card: abstract.Card):
        self.board.trigger('flip', card)
        card.face = 'up'

    def attack(self, attacker: abstract.Monster, defender: abstract.Monster):
        self.board.trigger('attack', attacker, defender)

        if defender.face == 'down':
            self.flip(defender)

        attacker_ATK = attacker.current__ATK
        defender_DEF = defender.current__DEF
        defender_ATK = defender.current__ATK

        if defender.position == 'defense':
            diff = attacker_ATK - defender_DEF
            if diff > 0:
                self.opponent.destroy(defender)
            if diff < 0:
                self.inflict_damage(-diff)
        
        if defender.position == 'attack':
            diff = attacker_ATK - defender_ATK
            if diff > 0:
                self.opponent.inflict_damage(diff)
                self.opponent.destroy(defender)
            if diff < 0:
                self.inflict_damage(diff)
                self.destroy(attacker)
            if diff == 0:
                self.opponent.destroy(defender)
                self.destroy(attacker)

    def send_to_graveyard(self, card: abstract.Card):
        # TODO stop checking, delegate the origin-place-event-trigger
        # of the card to higher-level behavior i.e. discard and destroy
        for _card in self.side_of_the_field():
            if _card == card:
                self.board.trigger('send-to-graveyard:from-field', card)
                self.remove_from_field(card)

        if card in self.hand:
            self.board.trigger('send-to-graveyard:from-hand', card)
            self.hand.remove(card)

        self.graveyard.append(card)

    # TODO def discard

    def banish(self, card: abstract.Card):
        self.board.trigger('banish', card)
        self.remove_from_field(card)
        self.hand.remove(card)
        self.deck.remove(card)
        self.banished.append(card)
