from typing import List, FrozenSet
from enum import Enum, auto

from . import abstract
from .abstract import Attribute

class Card(abstract.Card):
    def __init__(self, name: str, description: str, types: FrozenSet[str],
                 effects: List[abstract.Effect]) -> None:
        self.name = name
        self.description = description
        self.types = types
        self.effects = effects

        for effect in effects:
            effect.card = self

    def _get_current(self, attr: str):
        result = getattr(self, attr)
        for effect in self.targeters:
            result = getattr(effect, 'pipe__'+attr, lambda x: x)(result)
        return result

    @property
    def current__types(self) -> FrozenSet[str]:
        return self._get_current('types')

class Monster(Card, abstract.Monster):
    def __init__(self, name: str, description: str, types: FrozenSet[str],
                 ATK: int, DEF: int, level: int, attribute: Attribute,
                 effects: List[abstract.Effect] = []) -> None:
        if not (level >= 0 and level <= 12):
            raise ValueError('Level of monster must be an integer between'
                             ' 0 and 12')
        Card.__init__(self, name, description, types, effects)
        self.ATK = ATK
        self.DEF = DEF
        self.level = level
        self.attribute = attribute

        if not effects:
            self.types = self.types.union(['normal'])

    @property
    def current__ATK(self) -> int:
        return self._get_current('ATK')

    @property
    def current__DEF(self) -> int:
        return self._get_current('DEF')

    @property
    def current__level(self) -> int:
        return self._get_current('level')

    @property
    def current__attribute(self) -> abstract.Attribute:
        return self._get_current('attribute')

    def __repr__(self):
        return ('{ref}(name={name}, types={types}, ATK={ATK}, DEF={DEF}, '
                'level={level}, attribute={attribute}, description='
                '{description}, effects={effects})'.format(
            ref = type(self).__module__+'.'+type(self).__name__,
            name = repr(self.name),
            types = self.types,
            ATK = self.ATK,
            DEF = self.DEF,
            level = self.level,
            attribute = self.attribute,
            description = repr(self.description),
            effects = self.effects
        ))

class Spell(abstract.Spell, Card):
    def __init__(self, name: str, description: str, icon: str,
                 effects: List[abstract.Effect]) -> None:
        types = frozenset(['spell', icon])
        Card.__init__(self, name, description, types, effects)

class Trap(abstract.Trap, Card):
    def __init__(self, name: str, description: str, icon: str,
                 effects: List[abstract.Effect]) -> None:
        types = frozenset(['trap', icon])
        Card.__init__(self, name, description, types, effects)
