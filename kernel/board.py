from typing import List, Dict, Callable, FrozenSet

from . import abstract
from .side import Side

class Board(abstract.Board):
    def __init__(self, side_top: abstract.Side,
                 side_bot: abstract.Side) -> None:
        self.side_top = side_top
        side_top.board = self
        side_top.opponent = side_bot
        self.side_bot = side_bot
        side_bot.board = self
        side_bot.opponent = side_top
        self._callbacks: Dict[str, List[Callable]]

    def field(self):
        for card in self.side_top.side_of_the_field():
            yield card

        for card in self.side_bot.side_of_the_field():
            yield card

    def effectual_cards(self):
        for card in self.side_top.effectual_cards():
            yield card

        for card in self.side_bot.effectual_cards():
            yield card

    def listen(self, event_name: str, callback: Callable):
        if self._callbacks.get(event_name) == None:
            self._callbacks[event_name] = []
        self._callbacks[event_name].append(callback)

    def trigger(self, event_name: str, *args):
        for callback in self._callbacks.get(event_name, []):
            callback(*args)
