from typing import List, Set, Callable
from . import abstract

class Effect(abstract.Effect):
    def __init__(self, name: str, events: Set[str], check: Callable[[], bool],
                 resolve: Callable) -> None:
        self.name = name
        self.events = events
        self.check = check
        self.resolve = resolve
        self.negated = False
        self.targets = []

    @property
    def board(self):
        return self.card.side.board

    @property
    def side(self):
        return self.card.side

    def target(self, targets: List[abstract.Card]):
        self.board.trigger('target')
        self.targets.extend(targets)

    def re_target(self, new: abstract.Card, old: abstract.Card = None):
        self.board.trigger('target:re-target')
        self.targets.remove(old)
        self.targets.append(new)

    def remove_target(self, target: abstract.Card = None):
        self.targets.remove(target)
